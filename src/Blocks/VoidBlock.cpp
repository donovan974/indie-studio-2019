/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** VoidBlock
*/

#include "blocks/VoidBlock.hpp"

Indie::VoidBlock::VoidBlock() : Block::Block(0)
{
    _type = VOID;
}

Indie::VoidBlock::~VoidBlock()
{
}

void Indie::VoidBlock::setTextureVect(const std::vector<std::string> &textures)
{
    (void)textures;
}

void Indie::VoidBlock::init()
{
}

void Indie::VoidBlock::update()
{
}

irr::scene::IMeshSceneNode *Indie::VoidBlock::getNode()
{
    return nullptr;
}
