/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** input
*/

#include "KeyHandling.hpp"

using namespace io;
using namespace irr;
using namespace gui;
using namespace core;
using namespace scene;

Indie::KeyHandling::KeyHandling()
{
	for (u32 i = 0; i < KEY_KEY_CODES_COUNT; i++)
		KeyIsDown[i] = false;
}

Indie::KeyHandling::~KeyHandling()
{
}

bool Indie::KeyHandling::OnEvent(const SEvent &event)
{
	if (event.EventType == EET_KEY_INPUT_EVENT)
		KeyIsDown[event.KeyInput.Key] = event.KeyInput.PressedDown;
	return false;
}