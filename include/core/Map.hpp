/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** Map
*/

#ifndef MAP_HPP_
#define MAP_HPP_

#include "IndieStudio.hpp"

namespace Indie {

    #define MAP_SIZE                13
    #define OBSTACLE_PROBABILITY    5

    #define IS_TOP_LEFT(x, y, len)      ((y == 1 && x == 1) || (y == 1 && x == 2) || (y == 2 && x == 1))
    #define IS_TOP_RIGHT(x, y, len)     ((y == 1 && x == len - 2) || (y == 1 && x == len - 1) || (y == 2 && x == len - 1))
    #define IS_BOTTOM_LEFT(x, y, len)   ((y == len - 2 && x == 1) || (y == len - 1 && x == 1) || (y == len - 1 && x == 2))
    #define IS_BOTTOM_RIGHT(x, y, len)  ((y == len - 2 && x == len - 1) || (y == len - 1 && x == len - 1) || (y == len - 1 && x == len - 2))

    #define UNDESTRUCTIBLE_BLOCK    '#'
    #define DESTRUCTIBLE_BLOCK      'o'
    #define VOID_BLOCK              ' '

    class MapGenerator {
        public:

            MapGenerator(const MapGenerator &) = delete;
            MapGenerator &operator=(const MapGenerator &) = delete;
            ~MapGenerator() = default;

            static MapGenerator &get();
            static void generate();
            static const std::vector<std::string> &getContent();

            void generateImpl();
            const std::vector<std::string> &getContentImpl() const;

        private:

            MapGenerator();
            char generateCell(int y, int x) const;

        private:

            std::vector<std::string> _map;
    };

}

#endif