/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** Particles
*/

#include "core/Particles.hpp"
#include "core/Scene.hpp"

Indie::Particle::Particle(irr::scene::IParticleSystemSceneNode *irrParticle)
{
	_particle = irrParticle;
}

void Indie::ParticleManager::createParticle(const Particle::Type type, const irr::core::vector3df &position, unsigned int time_duration)
{
	auto tmp = position;
	tmp.Y = 0;
	_particlesMap.emplace_back(ParticleFactory::make_particle(type, tmp));
	_particlesMap.back()->setDuration(time_duration);
}

bool Indie::Particle::update()
{
	if (_time.getElapsedTimeAsMilliseconds() >= _time_duration) {
		_particle->remove();
		return true;
	}
	return false;
}

void Indie::ParticleManager::updateParticles()
{
	for (unsigned int i = 0; i != _particlesMap.size(); i+=1) {
		auto is_finished = _particlesMap[i]->update();
		if (is_finished) {
			_particlesMap.erase(_particlesMap.begin() + i);
			i -= 1;
		}
	}
}


std::shared_ptr<Indie::Particle> Indie::ParticleFactory::getBombSmoke(const irr::core::vector3df &position)
{
	irr::scene::IParticleSystemSceneNode* particleSystem = SceneContent::getSceneMgr()->addParticleSystemSceneNode(false);
	particleSystem->setPosition(position);

	irr::scene::IParticleEmitter* emitter = particleSystem->createBoxEmitter(
		irr::core::aabbox3d<irr::f32>(0,0,0,0,0,0), // coordonnees de la boite
		irr::core::vector3df(0.0f,0.03f,0.0f),      // direction de diffusion
		100,200,                                    // nb particules emises a la sec min / max
		irr::video::SColor(0,100,100,100),          // couleur la plus sombre
		irr::video::SColor(0,255,255,255),          // couleur la plus claire
		100, 600,                                   // duree de vie min / max
		0,                                          // angle max d'ecart / direction prevue
		irr::core::dimension2df(8.0f,8.0f),         // taille minimum
		irr::core::dimension2df(6.0f,6.0f)          // taille maximum
	);
	particleSystem->setEmitter(emitter);         	// on attache l'emetteur
	emitter->drop();                             	// plus besoin de ca

	irr::scene::IParticleAffector* affector = particleSystem->createFadeOutParticleAffector(irr::video::SColor(0,0,0,0), 1200);
	particleSystem->addAffector(affector);       	// ajout du modificateur au particle system
	affector->drop();                            	// plus besoin de ca

	particleSystem->setMaterialFlag(irr::video::EMF_LIGHTING, false);          // insensible a la lumiere
	particleSystem->setMaterialFlag(irr::video::EMF_ZWRITE_ENABLE, false);     // desactive zbuffer pour surfaces derriere
	particleSystem->setMaterialTexture(0, SceneContent::getDriver()->getTexture(FIRE_PARTICLE_PATH));
	particleSystem->setMaterialType(irr::video::EMT_TRANSPARENT_ADD_COLOR);

	auto pointer = std::make_shared<Particle>(particleSystem);
	return pointer;
}

std::shared_ptr<Indie::Particle> Indie::ParticleFactory::getPowerUpEffect(const irr::core::vector3df &position)
{
	irr::scene::IParticleSystemSceneNode* particleSystem = SceneContent::getSceneMgr()->addParticleSystemSceneNode(false);
	particleSystem->setPosition(position);

	irr::scene::IParticleEmitter* emitter = particleSystem->createBoxEmitter(
		irr::core::aabbox3d<irr::f32>(0,0,0,0,0,0), // coordonnees de la boite
		irr::core::vector3df(0.f,0.07f,0.f),      // direction de diffusion
		1000, 2000,                                 // nb particules emises a la sec min / max
		irr::video::SColor(100, 0, 255, 0),         // couleur la plus sombre
		irr::video::SColor(100, 150, 255, 150),     // couleur la plus claire
		1000, 2000,                                   // duree de vie min / max
		40,                                          // angle max d'ecart / direction prevue
		irr::core::dimension2df(0.60f,0.60f),         // taille minimum
		irr::core::dimension2df(0.60f,0.60f)          // taille maximum
	);
	particleSystem->setEmitter(emitter);         	// on attache l'emetteur
	emitter->drop();                             	// plus besoin de ca

	irr::scene::IParticleAffector* affector = particleSystem->createFadeOutParticleAffector(irr::video::SColor(0,0,0,0), 1200);
	particleSystem->addAffector(affector);       	// ajout du modificateur au particle system
	affector->drop();                            	// plus besoin de ca

	particleSystem->setMaterialFlag(irr::video::EMF_LIGHTING, false);          // insensible a la lumiere

	auto pointer = std::make_shared<Particle>(particleSystem);
	return pointer;
}


std::shared_ptr<Indie::Particle> Indie::ParticleFactory::make_particle(const Indie::Particle::Type type, const irr::core::vector3df &position)
{
	if (type == Indie::Particle::BOMB_SMOKE)
 		return getBombSmoke(position);
	if (type == Indie::Particle::POWER_UP_EFFECT)
		return getPowerUpEffect(position);
	throw Indie::Error("Cannot create object", __FILE__, __LINE__);
}
