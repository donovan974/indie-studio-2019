/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** Core
*/

#include "../include/Core.hpp"
#include "../include/tools/Args.hpp"

Indie::Core::Core(int ac, char **av)
    : _scene_facade(IScene::MainMenu)
{
    Indie::Args::compute(ac, av);
}

void Indie::Core::run()
{
    while (_scene_facade.isRunning()) {
        if (_scene_facade.getSceneState() != SceneContent::getSceneState())
            _scene_facade.changeScene(SceneContent::getSceneState());
        _scene_facade.update();
    }
}