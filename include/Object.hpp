/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** Object
*/

#ifndef OBJECT_HPP_
#define OBJECT_HPP_

namespace Indie {

    class IObject {
        public:

            virtual ~IObject() = default;
            virtual void init() = 0;
            virtual void update() = 0;
    };

}

#endif