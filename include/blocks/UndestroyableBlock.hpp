/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** UndestroyableBlock
*/

#ifndef UNDESTROYABLEBLOCK_HPP_
#define UNDESTROYABLEBLOCK_HPP_

#include "blocks/Block.hpp"

#define UNDESTROYABLE_TEXTURE_1 "../media/block.png"

namespace Indie {

class UndestroyableBlock : public Block{
    public:
        UndestroyableBlock();
        ~UndestroyableBlock();

		/**
		 * @brief init the MapDrawer Object
		 */
		void init();

		/**
		 * @brief Call every tick
		 */
		void update();

		/**
		 * @brief Get the Node in the scene manager.
		 *
		 * @return irr::scene::IMeshSceneNode*
		 */
		irr::scene::IMeshSceneNode *getNode();

		void swapToVoid() {}

    protected:
    private:
};

}

#endif
