/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** Args
*/

#include <cstddef>
#include <iostream>
#include "../../include/tools/Args.hpp"
#include "../../include/tools/Log.hpp"

void Indie::Args::compute(int ac, char** av)
{
    std::map<const std::string, const std::function<void()>> flags = init_flags();

    (void)ac;
    for (std::size_t i = 1; av[i]; i += 1) {
        if (flags.find(av[i]) != flags.end())
            flags[av[i]]();
        else
            std::cerr << ESCAPE_RED << av[i] << FLAG_ERROR << ESCAPE_END << std::endl;
    }
}

std::map<const std::string, const std::function<void()>> Indie::Args::init_flags()
{
    static bool is_setup = false;
    static std::map<const std::string, const std::function<void()>> flags;

    if (is_setup)
        return flags;

    flags.emplace(FLAG_DEBUG, [] { Log::state() = true; });

    is_setup = true;
    return flags;
}