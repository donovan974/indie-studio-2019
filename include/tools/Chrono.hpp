/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** Clock
*/

#ifndef CLOCK_HPP_
#define CLOCK_HPP_

#include <chrono>

namespace Indie {

    class Chrono {
        public:

            Chrono();
            ~Chrono() = default;

            int getElapsedTimeAsMilliseconds() const;
            int getElapsedTimeAsSeconds() const;
            void restart();

        private:
            std::chrono::steady_clock::time_point _start_time;
    };

}

#endif