IF (NOT SFML_INCLUDE_DIRS OR NOT SFML_LIBRARIES)
  FIND_PATH(SFML_INCLUDE_DIRS
    NAMES
      Audio.hpp
    PATHS
      /usr/include/SFML/        # Default Fedora28 system include path
      /usr/local/include/SFML/ default Fedora28 local include path
      ${CMAKE_SOURCE_DIR}/include/SFML-2.5.1/include/SFML/ # Expected to contain the path to this file for Windows10
      ${SFML_DIR}/include/SFML      # SFML root directory (if provided)
  )

  IF (MSVC)     # Windows
    SET(CMAKE_FIND_LIBRARY_PREFIXES "")
    SET(CMAKE_FIND_LIBRARY_SUFFIXES ".lib")
  ELSE (MSVC)   # Linux
    SET(CMAKE_FIND_LIBRARY_PREFIXES "lib")
    SET(CMAKE_FIND_LIBRARY_SUFFIXES ".so")
  ENDIF(MSVC)

  FIND_LIBRARY(SFML_LIBRARIES
    NAMES
      sfml-audio
    PATHS
      /usr/lib64/                   # Default Fedora28 library path
      /usr/lib/                     # Some more Linux library path
      /usr/lib/x86_64-linux-gnu/    # Some more Linux library path
      /usr/local/lib/               # Some more Linux library path
      /usr/local/lib64/             # Some more Linux library path
      ${CMAKE_SOURCE_DIR}/include/SFML-2.5.1/lib/     # Expected to contain the path to this file for Windows10
      ${SFML_DIR}/                  # SFML root directory (if provided)
  )
ENDIF (NOT SFML_INCLUDE_DIRS OR NOT SFML_LIBRARIES)

IF (SFML_INCLUDE_DIRS AND SFML_LIBRARIES)
  SET(SFML_FOUND TRUE)
ELSE (SFML_INCLUDE_DIRS AND SFML_LIBRARIES)
  SET(SFML_FOUND FALSE)
ENDIF (SFML_INCLUDE_DIRS AND SFML_LIBRARIES)

IF (SFML_FIND_REQUIRED AND NOT SFML_FOUND)
  MESSAGE(FATAL_ERROR
    "  SFML not found.\n"
    "      Windows: Fill CMake variable CMAKE_MODULE_PATH to the provided directory.\n"
    "      Linux: Install SFML using your package manager ($> sudo dnf install SFML).\n"
  )
ENDIF (SFML_FIND_REQUIRED AND NOT SFML_FOUND)