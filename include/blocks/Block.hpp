/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** Block
*/

#ifndef BLOCK_HPP_
#define BLOCK_HPP_

#include "IndieStudio.hpp"
#include "Object.hpp"

#define UNDESTORYABLE_MODEL_PATH "../media/fold/mis_obj_reproblock_SD.obj"
#define DESTORYABLE_MODEL_PATH "../media/model.dae"
#define WALL_MODEL_PATH " "

#define MAP_VOID_SYMBOLE ' '
#define MAP_UNDESTROYABLE_SYMBOLE '#'
#define MAP_DESTROYABLE_SYMBOLE 'o'

namespace Indie {

class Block : public IObject {
	public:

		enum Type {
			VOID = 0,
			UNDESTROYABLE,
			DESTROYABLE,
			BLOCK
		};

		Block(const std::size_t size);
		~Block();

        virtual void setTextureVect(const std::vector<std::string> &);
		virtual void init();
		virtual void update();
		virtual irr::scene::IMeshSceneNode *getNode();
		virtual void swapToVoid() {_type = VOID;}

		const Type &getBlockType();
		irr::scene::ITriangleSelector *getCollisionSelector() {return _collision_triangle;}
		void setCollisionSelector(irr::scene::ITriangleSelector *c) {_collision_triangle = c;}
		void setPosition(const irr::core::vector3df &position) {_position = position;}
		const irr::core::vector3df &getPosition() {return _position;}


	protected:
		irr::scene::ITriangleSelector *_collision_triangle;
		std::size_t _sizeCube;
		irr::scene::IMeshSceneNode *_cubeNode;
		std::vector<std::string> _textures;
		Type _type;
		irr::core::vector3df _position;

	private:
};

class BlockFactory {
	public:
		/**
		 * @brief create a block by using @param c
		 *
		 * @param c
		 * @return pointer to a block
		 */
		static std::shared_ptr<Block> make_block(const char c);

	protected:
	private:
		static void loadModel();
};

}

#endif
