/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** SfxBank
*/

#include "core/SfxBank.hpp"
#include "tools/Error.hpp"

Indie::SfxBank::SfxBank()
{
    _soundMemory.emplace(SFX_EXPLOSION, 	std::make_shared<sf::SoundBuffer>());
    _soundMemory.emplace(SFX_WALK, 			std::make_shared<sf::SoundBuffer>());
    _soundMemory.emplace(SFX_CLICK, 		std::make_shared<sf::SoundBuffer>());
    _soundMemory.emplace(SFX_MAIN_MENU, 	std::make_shared<sf::SoundBuffer>());
    _soundMemory.emplace(SFX_BG_GAME, 		std::make_shared<sf::SoundBuffer>());
    _soundMemory.emplace(SFX_POWER_UP, 		std::make_shared<sf::SoundBuffer>());
    _soundMemory.emplace(SFX_GET_REKT, 		std::make_shared<sf::SoundBuffer>());
    for (auto track : _soundMemory)
        track.second->loadFromFile(track.first);
}

std::shared_ptr<sf::SoundBuffer> &Indie::SfxBank::getSoundBuffer(const char *key)
{
    if (_soundMemory.find(key) == _soundMemory.end())
        throw Indie::Error("Cannot find this request", __FILE__, __LINE__);
    return _soundMemory[key];
}
