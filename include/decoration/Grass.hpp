/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** input
*/

#pragma once

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "Irrlicht.lib")
#endif

#include "IndieStudio.hpp"

using namespace irr;
using namespace gui;
using namespace core;
using namespace video;
using namespace scene;

#define GRASS_TEXTURE "../media/decoration/grass.jpg"
#define GRASS_OBJ_PATH "../media/decoration/grass.obj"

namespace Indie {
	class Grass {
		public:
			Grass();
			~Grass() {};
			void init();
			void SetPos();
		private:
			IAnimatedMesh* _mesh;
			IAnimatedMeshSceneNode* _node;
	};
}