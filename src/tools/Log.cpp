/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** Log
*/

#include "../../include/tools/Log.hpp"

Indie::Log& Indie::Log::get()
{
    static Log log;
    return log;
}

bool& Indie::Log::state()
{
    return get().stateImpl();
}

bool& Indie::Log::stateImpl()
{
    return _state;
}