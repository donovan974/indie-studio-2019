/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** Scene
*/

#include "core/Scene.hpp"
#include "core/Map.hpp"
#include "core/MeshLoader.hpp"
#include "core/SfxBank.hpp"
#include "decoration/Grass.hpp"

Indie::SceneContent &Indie::SceneContent::get()
{
    static SceneContent content;
    return content;
}

irr::IrrlichtDevice *&Indie::SceneContent::getDevice()
{
    return get().getDeviceImpl();
}

irr::video::IVideoDriver *&Indie::SceneContent::getDriver()
{
    return get().getDriverImpl();
}

irr::scene::ISceneManager *&Indie::SceneContent::getSceneMgr()
{
    return get().getSceneMgrImpl();
}

irr::gui::IGUIEnvironment *&Indie::SceneContent::getGUIEnv()
{
    return get().getGUIEnvImpl();
}

const Indie::KeyHandling &Indie::SceneContent::getKeyHandling()
{
    return get().getKeyHandlingImpl();
}

const Indie::IScene::SceneState &Indie::SceneContent::getSceneState()
{
    return get().getSceneStateImpl();
}

void Indie::SceneContent::setSceneState(const IScene::SceneState &new_state)
{
    get().setSceneStateImpl(new_state);
}

irr::IrrlichtDevice *&Indie::SceneContent::getDeviceImpl()
{
    return _irrDevice;
}

irr::video::IVideoDriver *&Indie::SceneContent::getDriverImpl()
{
    return _irrDriver;
}

irr::scene::ISceneManager *&Indie::SceneContent::getSceneMgrImpl()
{
    return _irrSceneMgr;
}

irr::gui::IGUIEnvironment *&Indie::SceneContent::getGUIEnvImpl()
{
    return _irrGUIEnv;
}

const Indie::KeyHandling &Indie::SceneContent::getKeyHandlingImpl() const
{
    return _keyHdl;
}

const Indie::IScene::SceneState &Indie::SceneContent::getSceneStateImpl() const
{
    return _state;
}

void Indie::SceneContent::setSceneStateImpl(const IScene::SceneState &new_state)
{
    _state = new_state;
}

void Indie::HelloWorldScene::init()
{
    Log::console("Hello World Scene init\n");
}

void Indie::HelloWorldScene::update()
{
    Log::console("Hello World Scene update\n");
}

void Indie::MainMenuScene::init()
{
    Indie::SfxBank::get();
    SceneContent &content = SceneContent::get();
    irr::gui::IGUIFont *font = content.getGUIEnvImpl()->getFont(MAIN_MENU_FONT_PATH);

    content.getGUIEnvImpl()->addImage(content.getDeviceImpl()->getVideoDriver()->getTexture(MAIN_MENU_BACKGROUND_PATH), irr::core::position2d<int>(0, 0));

    _audio.setBuffer(*SfxBank::get().getSoundBuffer(SFX_MAIN_MENU).get());
    _audio.setLoop(true);
    _audio.play();

    _buttonStart = content.getGUIEnvImpl()->addButton(irr::core::rect<irr::s32>(290,368,290 + 190,368 + 40), 0, -2, L"START");
    _buttonExit = content.getGUIEnvImpl()->addButton(irr::core::rect<irr::s32>(290,430, 290 + 190,430 + 40), 0, -2, L"EXIT");

    _buttonStart->setOverrideFont(font);
    _buttonExit->setOverrideFont(font);
}

void Indie::MainMenuScene::update()
{
    SceneContent &content = SceneContent::get();
    static bool is_start_pressed = false;
    static bool is_settings_pressed = false;
    static bool is_exit_pressed = false;

    if (is_start_pressed && _clock.getElapsedTimeAsMilliseconds() >= 50) {
        content.setSceneStateImpl(IScene::Game);
        is_start_pressed = false;
    }
    if (is_exit_pressed && _clock.getElapsedTimeAsMilliseconds() >= 50) {
        content.getDeviceImpl()->closeDevice();
        is_exit_pressed = false;
    }

    if (_buttonStart->isPressed()) {
        is_start_pressed = true;
        _clock.restart();
    }
    if (_buttonExit->isPressed()) {
        is_exit_pressed = true;
        _clock.restart();
    }
}

void Indie::GameScene::init()
{
    MapGenerator &map_canva = MapGenerator::get();
    MapDrawer &map_drawed = MapDrawer::get();

    map_canva.generate();
    map_drawed.load(map_canva.getContentImpl(), 5);
    map_drawed.init();

    _camera = SceneContent::getSceneMgr()->addCameraSceneNode(0, irr::core::vector3df{30, 52, 35} , irr::core::vector3df{30, 0, 30});

    CharacterContainer::init(1);

    _audio.setBuffer(*SfxBank::get().getSoundBuffer(SFX_BG_GAME).get());
    _audio.setLoop(true);
    _audio.play();
    Grass a;

    a.init();
}

void Indie::GameScene::update()
{
    SceneContent &content = SceneContent::get();

    content.getDriverImpl()->beginScene(true, true, irr::video::SColor(0,192,192,192));
    content.getDeviceImpl()->getCursorControl()->setVisible(false);
    CharacterContainer::update();
    MapDrawer::get().update();
}

std::unique_ptr<Indie::IScene> Indie::SceneFactory::create(const IScene::SceneState &scene_type)
{
    SceneFactoryMap factory_map = init();

    if (factory_map.find(scene_type) == factory_map.end())
        throw SceneError(ERR_SCENE_TYPE_UNKNOWN, __FILE__, __LINE__);
    return factory_map[scene_type]();
}

Indie::SceneFactory::SceneFactoryMap Indie::SceneFactory::init()
{
    static bool is_setup = false;
    static SceneFactoryMap factory_map;

    if (is_setup)
        return factory_map;
    factory_map.emplace(IScene::HelloWorld, [] { return std::make_unique<HelloWorldScene>();      });
    factory_map.emplace(IScene::MainMenu,   [] { return std::make_unique<MainMenuScene>();        });
    factory_map.emplace(IScene::Game,       [] { return std::make_unique<GameScene>();            });
    is_setup = true;
    return factory_map;
}

Indie::SceneFacade::SceneFacade(const IScene::SceneState &first_scene)
    : _current_scene(SceneFactory::create(first_scene)), _current_scene_state(first_scene)
{
    SceneContent &content = SceneContent::get();

    content.getDeviceImpl() = irr::createDevice(irr::video::EDT_OPENGL , irr::core::dimension2d<irr::u32>(WINDOW_WIDTH, WINDOW_HEIGHT), 16, false, false, false, (irr::IEventReceiver *)&content.getKeyHandlingImpl());
    content.getDeviceImpl()->setWindowCaption(WINDOW_TITLE);

    content.getDriverImpl() = content.getDeviceImpl()->getVideoDriver();
    content.getSceneMgrImpl() = content.getDeviceImpl()->getSceneManager();
    content.getGUIEnvImpl() = content.getDeviceImpl()->getGUIEnvironment();

    content.setSceneStateImpl(_current_scene_state);

    _current_scene->init();
}

Indie::SceneFacade::~SceneFacade()
{
    SceneContent::getDevice()->drop();
}

void Indie::SceneFacade::changeScene(const IScene::SceneState &new_scene)
{
    SceneContent &content = SceneContent::get();

    content.getSceneMgrImpl()->clear();
    content.getGUIEnvImpl()->clear();
    _current_scene = SceneFactory::create(new_scene);
    _current_scene_state = new_scene;
    _current_scene->init();
}

const Indie::IScene::SceneState &Indie::SceneFacade::getSceneState() const
{
    return _current_scene_state;
}

void Indie::SceneFacade::update()
{
    SceneContent &content = SceneContent::get();

    _current_scene->update();
    content.getSceneMgrImpl()->drawAll();
    content.getGUIEnvImpl()->drawAll();
    content.getDriverImpl()->endScene();
}

bool Indie::SceneFacade::isRunning()
{
    return SceneContent::getDevice()->run();
}