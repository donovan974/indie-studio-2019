/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** Keys
*/

#ifndef KEYS_HPP_
#define KEYS_HPP_

#include "IndieStudio.hpp"

namespace Indie {

    #define PLAYER_ONE              "Player1"
    #define P1_DEFAULT_KEY_UP       irr::KEY_KEY_Z
    #define P1_DEFAULT_KEY_DOWN     irr::KEY_KEY_S
    #define P1_DEFAULT_KEY_LEFT     irr::KEY_KEY_Q
    #define P1_DEFAULT_KEY_RIGHT    irr::KEY_KEY_D
    #define P1_DEFAULT_KEY_BOMB     irr::KEY_KEY_F

    #define PLAYER_TWO              "Player2"
    #define P2_DEFAULT_KEY_UP       irr::KEY_KEY_I
    #define P2_DEFAULT_KEY_DOWN     irr::KEY_KEY_K
    #define P2_DEFAULT_KEY_LEFT     irr::KEY_KEY_J
    #define P2_DEFAULT_KEY_RIGHT    irr::KEY_KEY_L
    #define P2_DEFAULT_KEY_BOMB     irr::KEY_KEY_M

    #define PLAYER_THREE            "Player3"
    #define PLAYER_FOUR             "Player4"

    class CharacterKeys {
        public:

            CharacterKeys(const char *player);
            CharacterKeys(const CharacterKeys &) = delete;
            CharacterKeys &operator=(const CharacterKeys &) = delete;
            ~CharacterKeys() = default;

            void changePreset(const char *player);
            void setKeyUp(const irr::EKEY_CODE &keycode);
            void setKeyDown(const irr::EKEY_CODE &keycode);
            void setKeyLeft(const irr::EKEY_CODE &keycode);
            void setKeyRight(const irr::EKEY_CODE &keycode);
            void setKeyBomb(const irr::EKEY_CODE &keycode);

            const irr::EKEY_CODE &getKeyUp() const;
            const irr::EKEY_CODE &getKeyDown() const;
            const irr::EKEY_CODE &getKeyLeft() const;
            const irr::EKEY_CODE &getKeyRight() const;
            const irr::EKEY_CODE &getKeyBomb() const;

            bool isPressed(const irr::EKEY_CODE &keycode) const;

        private:

            irr::EKEY_CODE _keyUp;
            irr::EKEY_CODE _keyDown;
            irr::EKEY_CODE _keyLeft;
            irr::EKEY_CODE _keyRight;
            irr::EKEY_CODE _keyBomb;
    };

}

#endif