/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** Scene
*/


#ifndef SCENE_HPP_
#define SCENE_HPP_

#include "IndieStudio.hpp"
#include "KeyHandling.hpp"
#include "character/Character.hpp"
#include "Collision.hpp"
#include "blocks/MapDrawer.hpp"
#include "core/SfxBank.hpp"

namespace Indie {

    class IScene : public IObject {
        public:

            enum SceneState {
                HelloWorld = 0,
                MainMenu,
                Game
            };

            virtual ~IScene() = default;
    };

    class SceneContent {
        public:

            SceneContent(const SceneContent &) = delete;
            SceneContent &operator=(const SceneContent &) = delete;
            ~SceneContent() = default;

            static SceneContent& get();
            static irr::IrrlichtDevice *&getDevice();
            static irr::video::IVideoDriver *&getDriver();
            static irr::scene::ISceneManager *&getSceneMgr();
            static irr::gui::IGUIEnvironment *&getGUIEnv();
            static const IScene::SceneState &getSceneState();
            static const KeyHandling &getKeyHandling();
            static void setSceneState(const IScene::SceneState &new_state);

            irr::IrrlichtDevice *&getDeviceImpl();
            irr::video::IVideoDriver *&getDriverImpl();
            irr::scene::ISceneManager *&getSceneMgrImpl();
            irr::gui::IGUIEnvironment *&getGUIEnvImpl();
            const IScene::SceneState &getSceneStateImpl() const;
            const KeyHandling &getKeyHandlingImpl() const;
            void setSceneStateImpl(const IScene::SceneState &new_state);

        private:

            SceneContent() = default;

        private:

            irr::IrrlichtDevice *_irrDevice;
            irr::video::IVideoDriver *_irrDriver;
            irr::scene::ISceneManager *_irrSceneMgr;
            irr::gui::IGUIEnvironment *_irrGUIEnv;
            KeyHandling _keyHdl;
            IScene::SceneState _state;
    };

    class HelloWorldScene : public IScene {
        public:

            HelloWorldScene() = default;
            HelloWorldScene(const HelloWorldScene &) = delete;
            HelloWorldScene &operator=(const HelloWorldScene &) = delete;
            ~HelloWorldScene() = default;

            void init();
            void update();
    };

    #define MAIN_MENU_BACKGROUND_PATH "../media/bomberman3.png"
    #define MAIN_MENU_FONT_PATH       "../media/fontlucida.png"


    class MainMenuScene : public IScene {
        public:

            MainMenuScene() = default;
            MainMenuScene(const MainMenuScene &) = delete;
            MainMenuScene &operator=(const MainMenuScene &) = delete;
            ~MainMenuScene() = default;

            void init();
            void update();

        private:

            sf::Sound _audio;
            irr::gui::IGUIButton *_buttonStart;
            irr::gui::IGUIButton *_buttonExit;
            Chrono _clock;
    };

    class GameScene : public IScene {
        public:

            GameScene() = default;
            GameScene(const GameScene &) = delete;
            GameScene &operator=(const GameScene &) = delete;
            ~GameScene() = default;

            void init();
            void update();

        private:

            sf::Sound _audio;
            irr::scene::ICameraSceneNode *_camera;
    };

    class SceneFactory {
        public:

            SceneFactory() = delete;
            SceneFactory(const SceneFactory &) = delete;
            SceneFactory &operator=(const SceneFactory &) = delete;
            ~SceneFactory() = delete;

            using SceneFactoryMap = std::map<IScene::SceneState, std::function<std::unique_ptr<IScene>()>>;

            static std::unique_ptr<IScene> create(const IScene::SceneState &scene_type);

        private:

            static SceneFactoryMap init();
    };

    class SceneFacade {
        public:

            SceneFacade(const IScene::SceneState &first_scene);
            SceneFacade(const SceneFacade &) = delete;
            SceneFacade &operator=(const SceneFacade &) = delete;
            ~SceneFacade();

            void changeScene(const IScene::SceneState &new_scene);
            const IScene::SceneState &getSceneState() const;
            bool isRunning();
            void update();

        private:

            std::unique_ptr<IScene> _current_scene;
            IScene::SceneState _current_scene_state;
    };

    #define WINDOW_TITLE    L"Bomberman"
    #define WINDOW_WIDTH    800
    #define WINDOW_HEIGHT   600

    class SceneError : public Error {
        public:

            SceneError(const char *msg, const char *file, int line)
                : Error(msg, file, line) {}
            ~SceneError() = default;
    };

    #define ERR_SCENE_TYPE_UNKNOWN "Scene type is unknown."

}

#endif