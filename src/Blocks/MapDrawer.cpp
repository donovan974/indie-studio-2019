/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** MapDrawer
*/

#include "blocks/MapDrawer.hpp"
#include "blocks/UndestroyableBlock.hpp"
#include "blocks/DestroyableBlock.hpp"
#include "blocks/VoidBlock.hpp"
#include "core/Scene.hpp"
#include "blocks/PowerUp.hpp"
#include "core/Particles.hpp"

void Indie::MapDrawer::init()
{
	irr::core::vector3df pos = _positionRef;

	for (auto line : _map) {
		for (std::size_t i = 0; i != line.size(); i += 1) {
			auto blockPointer = BlockFactory::make_block(line[i]);
			if (blockPointer == nullptr)
				throw MapDrawerError(ERR_NULL_POINTER, __FILE__, __LINE__ );
			blockPointer->init();
			blockPointer->setPosition(pos);
			if (blockPointer->getNode() != nullptr)
				blockPointer->getNode()->setPosition(pos);
			_blockMap.push_back(blockPointer);
			pos.X += _coef;
		}
		pos.Z += _coef;
		pos.X = _positionRef.X;
	}

    for (unsigned int i = 0; i != _blockMap.size(); i += 1) {
        if (_blockMap[i]->getBlockType() != Block::VOID) {
            auto collision = Collision::get().CreateCollision(_blockMap[i]->getNode());
            _blockMap[i]->setCollisionSelector(collision);
        }
    }
}

std::vector<std::shared_ptr<Indie::Block>> &Indie::MapDrawer::getAllBlock()
{
	return _blockMap;
}

void Indie::MapDrawer::update()
{
	Indie::ParticleManager::update();
}

irr::core::vector3df Indie::MapDrawer::getRightBlockPosition(const irr::core::vector3df &point)
{
	float offset = 2;
	irr::core::vector3df tmp = irr::core::vector3df{point.X + offset, point.Y, point.Z + offset};

	tmp.X = pointCalculator(tmp.X);
	tmp.Z = pointCalculator(tmp.Z);
	return tmp;
}

int Indie::MapDrawer::pointCalculator(const irr::f32 &coord)
{
	int x1 = 0;
	int x2 = 0;

	x1 = (int)coord / 10;
	x2 = (int)coord % 10;

	x1 *= 10;
	x2 = (x2 < 5) ? 0 : 5;
	x1 += x2;
	return x1;
}

void Indie::MapDrawer::load(const std::vector<std::string> &map, const std::size_t &coef)
{
	_map = map;
	_coef = coef;
}

Indie::MapDrawer &Indie::MapDrawer::get()
{
	static MapDrawer map_drawer;
	return map_drawer;
}

void Indie::MapDrawer::breakBlockAt(const irr::core::vector3df &position, const std::size_t &radius, bool is_piercing)
{
	bool isUpBreacked = false;
	bool isDownBreacked = false;
	bool isLeftBreacked = false;
	bool isRightBreacked = false;

	for (unsigned int i = 1; i != radius + 1; i += 1) {
		breakBlock(irr::core::vector3df{position.X, position.Y, position.Z});

		if (!isRightBreacked && breakBlock(irr::core::vector3df{position.X + (5 * i), position.Y, position.Z}))
			isRightBreacked = true;
		if (!isLeftBreacked && breakBlock(irr::core::vector3df{position.X - (5 * i), position.Y, position.Z}))
			isLeftBreacked = true;
		if (!isDownBreacked && breakBlock(irr::core::vector3df{position.X, position.Y, position.Z + (5 * i)}))
			isDownBreacked = true;
		if (!isUpBreacked && breakBlock(irr::core::vector3df{position.X, position.Y, position.Z - (5 * i)}))
			isUpBreacked = true;

		if (is_piercing) {
			isUpBreacked = false;
			isDownBreacked = false;
			isLeftBreacked = false;
			isRightBreacked = false;
		}
	}
}

bool Indie::MapDrawer::breakBlock(const irr::core::vector3df &position)
{
	CharacterContainer::checkDeath(position);
	for (unsigned int i = 0; i != _blockMap.size(); i += 1) {
		auto blockPosition = _blockMap[i]->getPosition();
		if (_blockMap[i]->getBlockType() != Block::UNDESTROYABLE && blockPosition.X == position.X &&  blockPosition.Z == position.Z) {
			Indie::ParticleManager::get().createParticle(Particle::BOMB_SMOKE, irr::core::vector3df(position.X, 0, position.Z), FIRE_PARTICLE_LIFE_TIME);
			if (_blockMap[i]->getBlockType() == Block::DESTROYABLE) {
				PowerUp::tryCreate(_blockMap[i]->getNode()->getPosition());
				_blockMap[i]->getNode()->setVisible(false);
				_blockMap[i]->swapToVoid();
				Collision::get().RemoveTriangleSelector(_blockMap[i]->getCollisionSelector());
				return true;
			}
		} else if (_blockMap[i]->getBlockType() == Block::UNDESTROYABLE && _blockMap[i]->getNode()->getPosition().X == position.X &&  _blockMap[i]->getNode()->getPosition().Z == position.Z)
			return true;
	}
	return false;
}