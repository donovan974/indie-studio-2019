/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** Clock
*/

#include "tools/Chrono.hpp"

Indie::Chrono::Chrono()
{
    _start_time = std::chrono::steady_clock::now();
}

int Indie::Chrono::getElapsedTimeAsMilliseconds() const
{
    std::chrono::steady_clock::time_point end_time = std::chrono::steady_clock::now();
    return (std::chrono::duration_cast<std::chrono::milliseconds>(end_time - _start_time).count());
}

int Indie::Chrono::getElapsedTimeAsSeconds() const
{
    std::chrono::steady_clock::time_point end_time = std::chrono::steady_clock::now();
    return (std::chrono::duration_cast<std::chrono::seconds>(end_time - _start_time).count());
}

void Indie::Chrono::restart()
{
    _start_time = std::chrono::steady_clock::now();
}