# INDIE STUDIO - 2019

[![pipeline status](https://gitlab.com/donovan974/indie-studio-2019/badges/master/pipeline.svg)](https://gitlab.com/donovan974/indie-studio-2019/-/commits/master)
[![coverage report](https://gitlab.com/donovan974/indie-studio-2019/badges/master/coverage.svg)](https://gitlab.com/donovan974/indie-studio-2019/-/commits/master)

Epitech project.
Creation of a full videogame based on the popular game Neo Bomberman

# Setup

## Fedora/Linux user

Open a terminal, navigate through the folder and create a **Build** folder, enter in it. Then run:

```bash
cmake ../ .
```

It will generate a **Makefile** you juste have to run 

```bash
make
```
and then you are ready to launch the game.

```bash
./bomberman
```

## Windows user

It's the same command as above for the _cmake_ except that it will generate a **bomberman.sln**

Open it in visual studio. And set up the project as **startup project** like the picture below. Then you can
build it in debug mode and launch the game. 

<img src="Settings.png"/>

If you want to relaunch it without using Visual studio, build the project in release mode. 
You will have an error, at this moment, you will have to launch the **LAUNCH_ME.bat**
located at the source of the folder. Now you can run the **bomberman.exe** from the Release folder.


# collaborators

- LALLEMAND Fabien-luc 
- FONTAINE Donovan
- MADI Abdillah
- SONGORO Odilya
- CADET Cédric
- POTTER Samuel


