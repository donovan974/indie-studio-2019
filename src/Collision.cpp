/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** input
*/

#include "Collision.hpp"
#include "core/Scene.hpp"
#include "character/Character.hpp"

using namespace io;
using namespace irr;
using namespace gui;
using namespace core;
using namespace scene;

Indie::Collision::Collision()
{
	selectorStock = Indie::SceneContent::getSceneMgr()->createMetaTriangleSelector();
}

Indie::Collision &Indie::Collision::get()
{
    static Collision collision;
    return collision;
}

irr::scene::ITriangleSelector *Indie::Collision::CreateCollision(IMeshSceneNode *node)
{
	ITriangleSelector* selector = Indie::SceneContent::getSceneMgr()->createTriangleSelector(node->getMesh(), node);

    selectorStock->addTriangleSelector(selector);
    node->setTriangleSelector(selector);
    return selector;
}

void Indie::Collision::SetCollision(IAnimatedMeshSceneNode *obj)
{
    const core::aabbox3d<f32>& hitbox = obj->getBoundingBox();
    vector3df radius = (vector3df)(hitbox.MaxEdge - hitbox.getCenter()) * 2.25;
    ISceneNodeAnimator *anim = Indie::SceneContent::getSceneMgr()->createCollisionResponseAnimator(selectorStock, obj, radius, irr::core::vector3df{ 0, 0, 0});

    obj->addAnimator(anim);
    anim->drop();
}

void Indie::Collision::RemoveTriangleSelector(irr::scene::ITriangleSelector *pointer)
{
	selectorStock->removeTriangleSelector(pointer);
}
