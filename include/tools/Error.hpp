/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** Error
*/

#ifndef ERROR_HPP_
#define ERROR_HPP_

#include <exception>
#include <iostream>
#include "tools/Log.hpp"

namespace Indie {

    class Error : public std::exception {
        public:

            Error(const char *msg, const char *file, int line)
                : _msg(msg), _file(file), _line(line) {}
            ~Error() = default;

        const char *what() const noexcept   { return _msg; }
        const char *where() const noexcept  { return _file; }
        int on() const noexcept             { return _line; }

        private:

            const char *_msg;
            const char *_file;
            int _line;
    };

    #define ERROR_DUMP(e) std::cout << ESCAPE_RED << e.where() << ':' << e.on() << ':' << e.what() << ESCAPE_END << std::endl;
    #define SOMETHING_WRONG "Something went wrong."

}

#endif