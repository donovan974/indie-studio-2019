/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** Block
*/

#include "blocks/Block.hpp"
#include "core/Scene.hpp"
#include "blocks/UndestroyableBlock.hpp"
#include "blocks/DestroyableBlock.hpp"
#include "blocks/VoidBlock.hpp"

Indie::Block::Block(const std::size_t size)
	: _sizeCube(size)
{
	_type = BLOCK;
}

Indie::Block::~Block()
{
}

void Indie::Block::setTextureVect(const std::vector<std::string> &textures)
{
	_textures = textures;
}

void Indie::Block::init()
{
	_cubeNode = SceneContent::getSceneMgr()->addCubeSceneNode(_sizeCube);
    _cubeNode->setMaterialFlag(irr::video::EMF_LIGHTING, false);
	for (auto texture : _textures)
		_cubeNode->setMaterialTexture(0, SceneContent::getDriver()->getTexture(texture.c_str()));
}

void Indie::Block::update()
{
}

const Indie::Block::Type &Indie::Block::getBlockType()
{
	return _type;
}

irr::scene::IMeshSceneNode *Indie::Block::getNode()
{
	return _cubeNode;
}

std::shared_ptr<Indie::Block> Indie::BlockFactory::make_block(const char c)
{
	switch (c) {
		case MAP_DESTROYABLE_SYMBOLE:
			return std::make_shared<DestroyableBlock>();
		case MAP_UNDESTROYABLE_SYMBOLE:
			return std::make_shared<UndestroyableBlock>();
		case MAP_VOID_SYMBOLE:
			return std::make_shared<VoidBlock>();
		default:
			return nullptr;
	}
	return nullptr;
}
