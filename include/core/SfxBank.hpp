/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** SfxBank
*/

#ifndef SFXBANK_HPP_
#define SFXBANK_HPP_

#include <map>
#include <memory>
#include "SFML-2.5.1/include/SFML/Audio.hpp"

#define SFX_EXPLOSION 	"../media/music/explosion.ogg"
#define SFX_CLICK     	"../media/music/click.ogg"
#define SFX_WALK      	"../media/music/walking.ogg"
#define SFX_MAIN_MENU	"../media/music/gamesound.ogg"
#define SFX_BG_GAME		"../media/music/gameBG.ogg"
#define SFX_POWER_UP	"../media/music/powerUp.ogg"
#define SFX_GET_REKT	"../media/music/getExplosed.ogg"

namespace Indie {

class SfxBank {
	public:
		~SfxBank() = default;
		SfxBank(const SfxBank &) = delete;
		SfxBank &operator=(const SfxBank &) = delete;

		static SfxBank &get() {static SfxBank s; return s;}

		std::shared_ptr<sf::SoundBuffer> &getSoundBuffer(const char *key);

	private:
		std::map<std::string, std::shared_ptr<sf::SoundBuffer>> _soundMemory;
		SfxBank();
};

}

#endif /* !SFXBANK_HPP_ */
