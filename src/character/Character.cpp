/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** input
*/

#include "core/Scene.hpp"
#include "character/Character.hpp"
#include "core/MeshLoader.hpp"
#include "blocks/PowerUp.hpp"

using namespace io;
using namespace irr;
using namespace gui;
using namespace core;
using namespace scene;

Indie::Character::Character(const char *player)
	: _keys(player), _isActive(false), _gui(player)
{
	_bombStock.emplace_back(BombFactory::create(IBomb::Default, nullptr));
}

f32 Indie::Character::TopLimitX(vector3df& velocity, f32 frameTime)
{
	if (velocity.X < 0) {
		velocity.X += MOVEMENT_SPEED * frameTime;
		velocity.X = velocity.X >= 0 ? 0.0f : velocity.X;
	} else {
		velocity.X -= MOVEMENT_SPEED * frameTime;
		velocity.X = velocity.X <= 0 ? 0.0f : velocity.X;
	}
	return velocity.X;
}

f32 Indie::Character::TopLimitZ(vector3df& velocity, f32 frameTime)
{
	if (velocity.Z < 0) {
		velocity.Z += MOVEMENT_SPEED * frameTime;
		velocity.Z = velocity.Z >= 0 ? 0.0f : velocity.Z;
	} else {
		velocity.Z -= MOVEMENT_SPEED * frameTime;
		velocity.Z = velocity.Z <= 0 ? 0.0f : velocity.Z;
	}
	return velocity.Z;
}

void Indie::Character::MoveHandling(KeyHandling keys, f32 frameDTime, vector3df& velocity)
{
	bool stock = _isAnime;
	if (!_isActive)
		return;
	if (_keys.isPressed(_keys.getKeyRight())) {
		_isAnime = true;
		_states.at(1) = true;
		velocity.X -= MOVEMENT_SPEED * frameDTime;
		velocity.X = velocity.X < -LIMITE ? -LIMITE : velocity.X;
	} else if (_keys.isPressed(_keys.getKeyLeft())) {
		_isAnime = true;
		_states.at(3) = true;
		velocity.X += MOVEMENT_SPEED * frameDTime;
		velocity.X = velocity.X > LIMITE ? LIMITE : velocity.X;
	} else {
		velocity.X = TopLimitX(velocity, frameDTime);
		_isAnime = false;
	}
	if (_isAnime == false) {
		if (_keys.isPressed(_keys.getKeyUp())) {
			_isAnime = true;
			_states.at(2) = true;
			velocity.Z -= MOVEMENT_SPEED * frameDTime;
			velocity.Z = velocity.Z < -LIMITE ? -LIMITE : velocity.Z;
		} else if (_keys.isPressed(_keys.getKeyDown())) {
			_isAnime = true;
			_states.at(0) = true;
			velocity.Z += MOVEMENT_SPEED * frameDTime;
			velocity.Z = velocity.Z > LIMITE ? LIMITE : velocity.Z;
		} else {
			velocity.Z = TopLimitZ(velocity, frameDTime);
			_isAnime = false;
		}
	}
	if (stock != _isAnime)
		_isAnime == true ? _node->setFrameLoop(0, 27) : _node->setFrameLoop(27, 76);
	if (_keys.isPressed(_keys.getKeyUp()) 		||
		_keys.isPressed(_keys.getKeyDown())		||
		_keys.isPressed(_keys.getKeyRight())	||
		_keys.isPressed(_keys.getKeyLeft()))
		_isWalking = true;
	else
		_isWalking = false;
}

void Indie::Character::RotateAction(vector3df& velocity, f32 frameTime)
{
	if (_states.at(0) == true) {
		RotationHandling(270);
		_states.at(0) = false;
	}
	if (_states.at(1) == true) {
		RotationHandling(0);
		_states.at(1) = false;
	}
	if (_states.at(2) == true) {
		RotationHandling(90);
		_states.at(2) = false;
	}
	if (_states.at(3) == true) {
		RotationHandling(180);
		_states.at(3) = false;
	}
}

void Indie::Character::RotationHandling(const int angle)
{
	switch (angle) {
		case 0:
			_node->setRotation(vector3df(0, 90, angle));
			break;
		case 180:
			_node->setRotation(vector3df(180, 270, angle));
			break;
		case 90:
			_node->setRotation(vector3df(0, 0, 0));
			break;
		case 270:
			_node->setRotation(vector3df(0, 180, 0));
	}
}

void Indie::Character::MovePlayer(IrrlichtDevice* my_device, KeyHandling keys)
{
	u32 now = my_device->getTimer()->getTime();
	f32 newFrame = (f32)(now - _then) / 1000.f;
	_then = now;
	static vector3df _vec = { 0, 0, 0 };

	if (_movementClock.getElapsedTimeAsMilliseconds() >= 5) {
		MoveHandling(keys, newFrame, _vec);
		RotateAction(_vec, newFrame);
		_node->setPosition(_node->getPosition() + _vec);
		_movementClock.restart();
	}
}

void Indie::Character::init()
{
	IrrlichtDevice* Device = Indie::SceneContent::getDevice();
	_hp = 100;
	_states = std::vector<bool>(4);
	_isAnime = false;
	_mesh = MeshLoader::getMesh(MeshLoader::BOMBERMAN_MODEL);
	_audio.setBuffer(*SfxBank::get().getSoundBuffer(SFX_WALK).get());
	_audio.setLoop(true);
	_gui.init();

	if (!_mesh) {
		Device->drop();
		throw CharacterError(ERR_MODEL, __FILE__, __LINE__);
	}

	_node = Device->getSceneManager()->addAnimatedMeshSceneNode(_mesh);

	if (_node) {
		_node->setMaterialTexture(0, Device->getVideoDriver()->getTexture(_texture));
		_node->setMaterialFlag(EMF_LIGHTING, false);
		_node->setFrameLoop(27, 76);
		_node->setAnimationSpeed(60);
		_node->setScale(vector3df(4, 4, 4));
	}
	else
		throw CharacterError(ERR_TEXTURE, __FILE__,__LINE__);
	_then = Device->getTimer()->getTime();

	for (auto &bomb : _bombStock)
		bomb->init();

	Collision::get().SetCollision(_node);

	RotationHandling(270);
}

void Indie::Character::update()
{
	static bool w = true;

	for (auto &bomb : _bombStock)
		bomb->update();
	if (!_isalive)
		return;
	MovePlayer(SceneContent::getDevice(), Indie::SceneContent::getKeyHandling());
	placeBomb(SceneContent::getKeyHandling());
	PowerUp::check(this);
	if (_isWalking && w) {
		_audio.play();
		w = false;
	}
	else if (!_isWalking) {
		_audio.stop();
		w = true;
	}
	_gui.update();
	_gui.setBomNumber(getBombNumber());
	_gui.setBomRadius(_bombStock[0]->getRadius());
}

void Indie::Character::placeBomb(KeyHandling keys)
{
	static bool is_bomb_key_pressed = false;
	static bool is_bomb_key_released = false;

	if (is_bomb_key_released)
		is_bomb_key_released = false;
	if (!is_bomb_key_pressed && _keys.isPressed(_keys.getKeyBomb()))
		is_bomb_key_pressed = true;
	if (is_bomb_key_pressed && !_keys.isPressed(_keys.getKeyBomb())) {
		is_bomb_key_pressed = false;
		is_bomb_key_released = true;
	}

	if (is_bomb_key_released) {
		for (auto &bomb : _bombStock) {
			if (!bomb->isDroped()) {
				bomb->drop(_node->getPosition());
				return;
			}
		}
	}
}

void Indie::Character::getKilled()
{
	if (!_isalive)
		return;
	_audio.stop();
	_audio.setLoop(false);
	_audio.setBuffer(*SfxBank::get().getSoundBuffer(SFX_GET_REKT).get());
	_audio.play();
	_isalive = false;
	_isWalking = false;
	_node->setVisible(false);
}

void Indie::Character::bombLevelUp(const IBomb::BombType &bombType)
{
	for (std::shared_ptr<IBomb> &bomb : _bombStock)
		bomb = BombFactory::create(bombType, bomb);
}

void Indie::Character::addBomb(const std::size_t &nb_bomb)
{
	if (_bombStock.size() < MAX_BOMB) {
		for (unsigned int i = 0; i != nb_bomb; i += 1) {
			std::shared_ptr<IBomb> tmp;
			tmp.reset(_bombStock.back()->clone());
			_bombStock.emplace_back(tmp);
			_bombStock.back()->init();
			if (_bombStock.back()->isDroped())
				_bombStock.back()->recover();
		}
	}
}

Indie::CharacterContainer &Indie::CharacterContainer::get()
{
	static CharacterContainer container;
	return container;
}

void Indie::CharacterContainer::init(std::size_t nb_player)
{
	get().initImpl(nb_player);
}

void Indie::CharacterContainer::update()
{
	get().updateImpl();
}

void Indie::CharacterContainer::checkDeath(const irr::core::vector3df &position)
{
	get().checkDeathImpl(position);
}

void Indie::CharacterContainer::initImpl(std::size_t nb_player)
{
	const char *tmp[] = { PLAYER_ONE, PLAYER_TWO, nullptr };

	for (unsigned int i = 0; i != nb_player && i != MAX_PLAYER; i += 1) {
		_characters.emplace_back(Entity{ std::make_shared<Character>(tmp[i]), true });
		_characters.back().character->init();
		_characters.back().character->setPosition(getStartPosition(tmp[i]));
	}
}

void Indie::CharacterContainer::updateImpl()
{
	if (_chrono.getElapsedTimeAsMilliseconds() >= 2500)
		for (unsigned int i = 0; i != _characters.size(); i += 1) {
			if (_chrono.getElapsedTimeAsMilliseconds() >= 2500)
				_characters[i].character->activeMovement(true);
			_characters[i].character->update();
		}
}

void Indie::CharacterContainer::checkDeathImpl(const irr::core::vector3df &position)
{
	for (unsigned int i = 0; i != _characters.size(); i += 1) {
		auto character_position = MapDrawer::getRightBlockPosition(_characters[i].character->getPosition());
		if (character_position.X == position.X && character_position.Z == position.Z)
			_characters[i].character->getKilled();
	}
}

irr::core::vector3df Indie::CharacterContainer::getStartPosition(const char *player)
{
	if (strcmp(player, PLAYER_ONE) == 0)
		return PLAYER_ONE_START_POSITION;
	if (strcmp(player, PLAYER_TWO) == 0)
		return PLAYER_TWO_START_POSITION;
	throw CharacterError(ERR_PLAYER_UNKNOWN, __FILE__, __LINE__);
}

Indie::CharacterGUI::CharacterGUI(const char *key)
	:	_radiusNumber(1), _bombNumber(1)
{
	if (strcmp(key, PLAYER_ONE) == 0)
		_origin = PLAYER_ONE_GUI_POSITION;
	else if (strcmp(key, PLAYER_TWO) == 0)
		_origin = PLAYER_TWO_GUI_POSITION;
	else if (strcmp(key, PLAYER_THREE) == 0)
		_origin = PLAYER_THREE_GUI_POSITION;
	else if (strcmp(key, PLAYER_FOUR) == 0)
		_origin = PLAYER_FOUR_GUI_POSITION;
	else
		throw Indie::Error("WrongKey.", __FILE__, __LINE__);
}

void Indie::CharacterGUI::init()
{
	auto tmp = _origin;
	auto font = SceneContent::getGUIEnv()->getFont("../media/fontlucida.png");
	irr::core::vector2di imagePos(tmp.UpperLeftCorner.X + 20, tmp.UpperLeftCorner.Y - 25);

	_bombLabel = SceneContent::getGUIEnv()->addStaticText(L"0", tmp, false, true, 0, -1, true);
	_bombLabel->setOverrideFont(font);

    SceneContent::getGUIEnv()->addImage(SceneContent::getDriver()->getTexture(GFX_BOMB), imagePos);
	tmp.LowerRightCorner.Y += 50;
	tmp.UpperLeftCorner.Y  += 50;

	imagePos = irr::core::vector2di(tmp.UpperLeftCorner.X + 20, tmp.UpperLeftCorner.Y - 25);
	_radiusLabel = SceneContent::getGUIEnv()->addStaticText(L"0", tmp, false, true, 0, -1, true);
	_radiusLabel->setOverrideFont(font);
    SceneContent::getGUIEnv()->addImage(SceneContent::getDriver()->getTexture("../media/fire_flower_logo.png"), imagePos);
}

void Indie::CharacterGUI::update()
{
	std::wstring tmp;

	tmp = std::to_wstring(_bombNumber);
	_bombLabel->setText(tmp.c_str());
	tmp = std::to_wstring(_radiusNumber);
	_radiusLabel->setText(tmp.c_str());
}
