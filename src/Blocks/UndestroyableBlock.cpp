/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** UndestroyableBlock
*/

#include "blocks/UndestroyableBlock.hpp"

Indie::UndestroyableBlock::UndestroyableBlock() :
    Block::Block(5)
{
    _type = UNDESTROYABLE;
    _textures.emplace_back(UNDESTROYABLE_TEXTURE_1);
}

Indie::UndestroyableBlock::~UndestroyableBlock()
{
}

void Indie::UndestroyableBlock::init()
{
	Block::init();
}

void Indie::UndestroyableBlock::update()
{
	Block::update();
}

irr::scene::IMeshSceneNode *Indie::UndestroyableBlock::getNode()
{
    return Block::getNode();
}