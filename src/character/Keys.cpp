/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** Keys
*/

#include <cstring>
#include "character/Keys.hpp"
#include "core/Scene.hpp"

Indie::CharacterKeys::CharacterKeys(const char *player)
{
    changePreset(player);
}

void Indie::CharacterKeys::changePreset(const char *player)
{
    if (strcmp(player, PLAYER_ONE) == 0) {
        _keyUp = P1_DEFAULT_KEY_UP;
        _keyDown = P1_DEFAULT_KEY_DOWN;
        _keyLeft = P1_DEFAULT_KEY_LEFT;
        _keyRight = P1_DEFAULT_KEY_RIGHT;
        _keyBomb = P1_DEFAULT_KEY_BOMB;
        return;
    }
    if (strcmp(player, PLAYER_TWO) == 0) {
        _keyUp = P2_DEFAULT_KEY_UP;
        _keyDown = P2_DEFAULT_KEY_DOWN;
        _keyLeft = P2_DEFAULT_KEY_LEFT;
        _keyRight = P2_DEFAULT_KEY_RIGHT;
        _keyBomb = P2_DEFAULT_KEY_BOMB;
        return;
    }
    changePreset(PLAYER_ONE);
}

void Indie::CharacterKeys::setKeyUp(const irr::EKEY_CODE &keycode)
{
    _keyUp = keycode;
}

void Indie::CharacterKeys::setKeyDown(const irr::EKEY_CODE &keycode)
{
    _keyDown = keycode;
}

void Indie::CharacterKeys::setKeyLeft(const irr::EKEY_CODE &keycode)
{
    _keyLeft = keycode;
}

void Indie::CharacterKeys::setKeyRight(const irr::EKEY_CODE &keycode)
{
    _keyRight = keycode;
}

void Indie::CharacterKeys::setKeyBomb(const irr::EKEY_CODE &keycode)
{
    _keyBomb = keycode;
}

const irr::EKEY_CODE &Indie::CharacterKeys::getKeyUp() const
{
    return _keyUp;
}

const irr::EKEY_CODE &Indie::CharacterKeys::getKeyDown() const
{
    return _keyDown;
}

const irr::EKEY_CODE &Indie::CharacterKeys::getKeyLeft() const
{
    return _keyLeft;
}

const irr::EKEY_CODE &Indie::CharacterKeys::getKeyRight() const
{
    return _keyRight;
}

const irr::EKEY_CODE &Indie::CharacterKeys::getKeyBomb() const
{
    return _keyBomb;
}

bool Indie::CharacterKeys::isPressed(const irr::EKEY_CODE &keycode) const
{
    return SceneContent::getKeyHandling().IsKeyDown(keycode);
}