/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** main
*/

#include <ctime>
#include "IndieStudio.hpp"
#include "Core.hpp"

int main(int ac, char** av)
{
    std::srand(std::time(nullptr));
    try {
        Indie::Core core(ac, av);
        core.run();
    } catch (const Indie::Error &e) {
        ERROR_DUMP(e);
        return __EXIT_FAILURE__;
    }
    return(__EXIT_SUCCESS__);
}