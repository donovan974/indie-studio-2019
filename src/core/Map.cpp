/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** Map
*/

#include "core/Map.hpp"

Indie::MapGenerator::MapGenerator()
{
    _map.resize(MAP_SIZE);
    for (std::string &lines : _map)
        lines.resize(MAP_SIZE);
}

Indie::MapGenerator &Indie::MapGenerator::get()
{
    static MapGenerator map;
    return map;
}

void Indie::MapGenerator::generate()
{
    get().generateImpl();
}

const std::vector<std::string> &Indie::MapGenerator::getContent()
{
    return get().getContentImpl();
}

void Indie::MapGenerator::generateImpl()
{
    for (unsigned int y = 0; y != MAP_SIZE; y += 1) {
        for (unsigned int x = 0; x != MAP_SIZE; x += 1) {
            if (y == 0 || y == _map.size() - 1)
                _map[y][x] = UNDESTRUCTIBLE_BLOCK;
            else {
                if (x == 0 || x == _map.size() - 1)
                    _map[y][x] = UNDESTRUCTIBLE_BLOCK;
                else
                    _map[y][x] = y % 2 == 0 && x % 2 == 0 ? UNDESTRUCTIBLE_BLOCK : generateCell(y, x);
            }
        }
    }
}

const std::vector<std::string> &Indie::MapGenerator::getContentImpl() const
{
    return _map;
}

char Indie::MapGenerator::generateCell(int y, int x) const
{
    if (IS_TOP_LEFT(x, y, MAP_SIZE - 1) || IS_TOP_RIGHT(x, y, MAP_SIZE - 1) ||
    IS_BOTTOM_LEFT(x, y, MAP_SIZE - 1) || IS_BOTTOM_RIGHT(x, y, MAP_SIZE - 1))
        return VOID_BLOCK;
    return std::rand() % OBSTACLE_PROBABILITY == 0 ? VOID_BLOCK : DESTRUCTIBLE_BLOCK;
}