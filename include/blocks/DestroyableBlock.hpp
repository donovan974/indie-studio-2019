/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** DestroyableBlock
*/

#ifndef DESTROYABLEBLOCK_HPP_
#define DESTROYABLEBLOCK_HPP_

#include "blocks/Block.hpp"

#define DESTROYABLE_TEXTURE_1 "../media/object_nm01_BlockBrick001_spc.png"
#define DESTROYABLE_TEXTURE_2 "../media/object_sm01_BlockBrick001_dif_0.png"

namespace Indie {

class DestroyableBlock : public Block {
    public:
        DestroyableBlock();
        ~DestroyableBlock();

		/**
		 * @brief init the MapDrawer Object
		 */
		void init();

		/**
		 * @brief Call every tick
		 */
		void update();

		/**
		 * @brief Get the Node in the scene manager.
		 *
		 * @return irr::scene::IMeshSceneNode*
		 */
		irr::scene::IMeshSceneNode *getNode();

    protected:
    private:
};

}

#endif
