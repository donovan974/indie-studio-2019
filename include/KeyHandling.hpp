#pragma once
/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** input
*/

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "Irrlicht.lib")
#endif

#include <irrlicht.h>
#include "driverChoice.h"

using namespace irr;

namespace Indie {

	class KeyHandling : public IEventReceiver
	{
		public:
			KeyHandling();
			~KeyHandling();
			bool OnEvent(const SEvent &);
			bool IsKeyDown(EKEY_CODE keyCode) const { return KeyIsDown[keyCode]; }

		private:
			bool KeyIsDown[KEY_KEY_CODES_COUNT];
	};

}