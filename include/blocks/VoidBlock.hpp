/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** VoidBlock
*/

#ifndef VOIDBLOCK_HPP_
#define VOIDBLOCK_HPP_

#include "blocks/Block.hpp"

namespace Indie {

class VoidBlock : public Block{
    public:
        VoidBlock();
        ~VoidBlock();

        /**
         * @brief Set the vector of texture to apply on the object
         */
        void setTextureVect(const std::vector<std::string> &);

		/**
		 * @brief init the MapDrawer Object
		 */
		void init();

		/**
		 * @brief Call every tick
		 */
		void update();

		/**
		 * @brief Get the Node in the scene manager.
		 *
		 * @return irr::scene::IMeshSceneNode*
		 */
        irr::scene::IMeshSceneNode *getNode();

    protected:
    private:
};

}

#endif /* !VOIDBLOCK_HPP_ */
