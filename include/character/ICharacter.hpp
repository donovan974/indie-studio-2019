#pragma once
/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** input
*/

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "Irrlicht.lib")
#endif

#include "IndieStudio.hpp"
#include "KeyHandling.hpp"

#define LIMITE 0.15f
#define BOMBERMAN_PATH "../media/Bomberman.MD3"

using namespace io;
using namespace irr;
using namespace gui;
using namespace core;
using namespace video;
using namespace scene;

namespace Indie {

	class ICharacter : public IObject {
		public:
			virtual ~ICharacter() {};
			virtual bool CheckLife() const = 0;
			virtual void RotationHandling(const int) = 0;
			virtual void RotateAction(vector3df& velocity, f32 frameTime) = 0;

			virtual void setHp(int) = 0;
			virtual void setSpeed(int) = 0;
			virtual void setPosition(vector3df) = 0;

			virtual int getHp() const = 0;
			virtual int getSpeed() const = 0;
			virtual unsigned int getBombNumber() = 0;
			virtual vector3df getPosition() const = 0;

			virtual void activeMovement(bool isActve) = 0;
			virtual void getKilled() = 0;
	};
}