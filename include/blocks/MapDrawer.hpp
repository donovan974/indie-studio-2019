/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** MapDrawer
*/

#ifndef MAPDRAWER_HPP_
#define MAPDRAWER_HPP_

#include "blocks/Block.hpp"

#define FIRE_PARTICLE_LIFE_TIME 200

namespace Indie {

class MapDrawer : public IObject{
	public:
		~MapDrawer() = default;

		void init();
		void update();
		void load(const std::vector<std::string> &, const std::size_t &);
		std::vector<std::shared_ptr<Block>> &getAllBlock();
		void breakBlockAt(const irr::core::vector3df &position, const std::size_t &radius, bool is_piercing);

		static irr::core::vector3df getRightBlockPosition(const irr::core::vector3df &relatif_position);
		static MapDrawer &get();
		static irr::scene::IParticleSystemSceneNode *addEffectOnPosition(const irr::core::vector3df &position);

	protected:

	private:
		MapDrawer() = default;
		bool breakBlock(const irr::core::vector3df &position);

	private:

		static int pointCalculator(const irr::f32 &pos);
		std::size_t _coef;
		std::vector<std::string> _map;
		std::vector<std::shared_ptr<Block>> _blockMap;
		irr::core::vector3df _positionRef;
};

class MapDrawerError : public Error {
	public:

		MapDrawerError(const char *msg, const char *file, int line)
			: Error(msg, file, line) {}
		~MapDrawerError() = default;
};

#define ERR_NULL_POINTER	"Null pointer."

}

#endif
