/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** Core
*/

#ifndef CORE_HPP_
#define CORE_HPP_

#include "IndieStudio.hpp"
#include "core/Scene.hpp"

namespace Indie {

    class Core {
        public:

            Core(int ac, char **av);
            Core(const Core &) = delete;
            Core &operator=(const Core &) = delete;
            ~Core() = default;

            void run();

        private:

            SceneFacade _scene_facade;
    };

};

#endif