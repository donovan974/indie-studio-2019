/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** Args
*/

#ifndef ARGS_HPP_
#define ARGS_HPP_

#include <map>
#include <functional>

namespace Indie {

    #define FLAG_DEBUG "-D"

    #define FLAG_ERROR ":flag unknown"

    class Args {
        public:

            Args() = delete;
            Args(const Args&) = delete;
            Args& operator=(const Args&) = delete;
            ~Args() = delete;

            /**
             * @brief Compute program arguments
             *
             * @param ac argument number
             * @param av arguments
             */
            static void compute(int ac, char** av);

        private:

            // Initialisation of the flag map
            static std::map<const std::string, const std::function<void()>> init_flags();
    };

}

#endif