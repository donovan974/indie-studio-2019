/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** IndieStudio
*/

#ifndef INDIESTUDIO_HPP_
#define INDIESTUDIO_HPP_

// frequently use header files
#include <iostream>
#include <vector>
#include <map>
#include <memory>
#include <functional>
#include <string>

// Irrlicht header files
#include <irrlicht.h>

// local pratical header files
#include "tools/Log.hpp"
#include "tools/Error.hpp"
#include "Object.hpp"

#define __EXIT_SUCCESS__ 0
#define __EXIT_FAILURE__ 84

#endif