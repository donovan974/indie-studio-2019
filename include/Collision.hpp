/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** input
*/

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "Irrlicht.lib")
#endif

#ifndef COLLISION_H_
#define COLLISION_H_

#include "IndieStudio.hpp"

using namespace irr;
using namespace gui;
using namespace core;
using namespace scene;

namespace Indie {

	class Collision {
		public:

			~Collision() = default;

			static Collision &get();

			ITriangleSelector *CreateCollision(IMeshSceneNode*);
			void SetCollision(IAnimatedMeshSceneNode*);
			void RemoveTriangleSelector(ITriangleSelector *);

		private:

			Collision();

		private:

			IMetaTriangleSelector* selectorStock;
	};
}

#endif