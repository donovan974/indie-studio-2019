/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** PowerUp
*/

#ifndef POWERUP_HPP_
#define POWERUP_HPP_

#include "IndieStudio.hpp"
#include "character/Character.hpp"
#include "core/SfxBank.hpp"

namespace Indie {

    #define POWER_UP_PROBA  3
    #define POWER_UP_SIZE   2
    #define FLOWER_TEXTURE "../media/Fire Flower/I_fireflower.png"

    class PowerUp {
        public:

            enum PowerUpType {
                RadiusUp = 0,
                AddBomb
            };

            PowerUp(const PowerUp &) = delete;
            PowerUp &operator=(const PowerUp &) = delete;
            ~PowerUp() = default;

            static PowerUp &get();
            static void tryCreate(const irr::core::vector3df &blockPosition);
            static void check(Character *player);

            void tryCreateImpl(const irr::core::vector3df &blockPosition);
            void checkImpl(Character *player);

        private:

            PowerUp() = default;

            void choosePowerUp(const irr::core::vector3df &blockPosition);
            void addPowerUp(Character *player, unsigned int index);

        private:
            sf::Sound _audio;
            std::vector<irr::scene::IAnimatedMeshSceneNode *> _powerUps;
            std::vector<PowerUpType> _types;
    };

    class PowerUpError : public Error {
        public:

            PowerUpError(const char *msg, const char *file, int line)
                : Error(msg, file, line) {}
            ~PowerUpError() = default;
    };

}

#endif