/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** MeshLoader
*/

#include "core/Scene.hpp"
#include "character/Bomb.hpp"
#include "core/MeshLoader.hpp"
#include "decoration/Grass.hpp"
#include "character/ICharacter.hpp"


Indie::MeshLoader::MeshLoader()
{
}

irr::scene::IAnimatedMesh *Indie::MeshLoader::getMesh(const Indie::MeshLoader::Type &type)
{
	static MeshLoader mesh;

	mesh.load();
	if (mesh._meshContainer.find(type) == mesh._meshContainer.end())
		throw Indie::ErrorMeshLoader(CANNOT_FIND_MESH, __FILE__, __LINE__);
	return mesh._meshContainer[type];
}

void Indie::MeshLoader::load()
{
	static bool _isLoaded = false;

	if (_isLoaded)
		return;
	_isLoaded = true;
	_meshContainer.emplace(std::make_pair(Indie::MeshLoader::BOMB, Indie::SceneContent::getSceneMgr()->getMesh(BOMB_OBJECT_PATH)));
	_meshContainer.emplace(std::make_pair(Indie::MeshLoader::BOMBERMAN_MODEL, Indie::SceneContent::getSceneMgr()->getMesh(BOMBERMAN_PATH)));
	_meshContainer.emplace(std::make_pair(Indie::MeshLoader::FLOWER_MODEL, Indie::SceneContent::getSceneMgr()->getMesh(FIRE_FLOWER)));
	_meshContainer.emplace(std::make_pair(Indie::MeshLoader::GRASS, Indie::SceneContent::getSceneMgr()->getMesh(GRASS_OBJ_PATH)));
	for (auto &obj : _meshContainer)
		obj.second->setMaterialFlag(EMF_LIGHTING, false);
}
