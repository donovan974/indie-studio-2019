/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** MeshLoader
*/

#ifndef MESHLOADER_HPP_
#define MESHLOADER_HPP_

#include "IndieStudio.hpp"
#include "tools/Error.hpp"

#define CANNOT_FIND_MESH "Cannot find the requested mesh"
#define FIRE_FLOWER "../media/Fire Flower/I_fireflower.obj"

namespace Indie {

class MeshLoader {
	public:

		enum Type {
			BOMB = 0,
			BOMBERMAN_MODEL,
			FLOWER_MODEL,
			GRASS
		};

		MeshLoader &operator=(const MeshLoader) = delete;
		MeshLoader(const MeshLoader&) = delete;
		~MeshLoader() = default;
		static irr::scene::IAnimatedMesh *getMesh(const Type&);

	protected:
	private:
		void load();
		MeshLoader();
		std::vector<char *> _pathMesh;
		std::map<Type, irr::scene::IAnimatedMesh *> _meshContainer;

};

class ErrorMeshLoader : public Error {
	public:
		ErrorMeshLoader(const char *msg, const char *file, int line)
			: Error(msg, file, line) {}
		~ErrorMeshLoader() = default;

	protected:
	private:
};

}

#endif
