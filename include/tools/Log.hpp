/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** Log
*/

#ifndef LOG_HPP_
#define LOG_HPP_

#include <iostream>

namespace Indie {

    #define ESCAPE_END "\033[0m"
    #define ESCAPE_RED "\x1B[91m"
    #define ESCAPE_BLUE "\x1B[34m"

    class Log {
        public:

            Log(const Log&) = delete;
            Log& operator=(const Log&) = delete;
            ~Log() = default;

            /**
             * @brief Return the Log object
             *
             * @return Log& Log object
             */
            static Log& get();

            /**
             * @brief Return the state of the Log object
             *
             * @return true log system is on
             * @return false log system is off
             */
            static bool& state();

            /**
             * @brief Print arguments to the console
             *
             * @tparam P1 argument type to print
             * @tparam Args more argument types (can be left empty)
             * @param p1 argument to print
             * @param args more arguments (can be left empty)
             */
            template <class P1, class... Args>
            static void console(const P1& p1, const Args&... args)
            {
                get().consoleImpl(p1, args...);
            }

        private:

            Log() = default;

            // Implementation of the 'state' method
            bool& stateImpl();

            // Implementation of the 'console' method
            template <class P1, class... Args>
            void consoleImpl(const P1& p1, const Args&... args) const
            {
                if (!_state)
                    return;

                std::cout << ESCAPE_BLUE << p1 << ESCAPE_END;
                consoleImpl(args...);
            }
            void consoleImpl() const {}

        private:

            bool _state = true;
    };

}

#endif