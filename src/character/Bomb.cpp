/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** Bomb
*/

#include "character/Bomb.hpp"
#include "core/Scene.hpp"
#include "core/MeshLoader.hpp"
#include "core/Particles.hpp"

Indie::Bomb::Bomb(const Bomb &other)
{
    *this = other;
}

Indie::Bomb &Indie::Bomb::operator=(const Bomb &other)
{
    _is_droped = other._is_droped;
    _radius = other._radius;
    _is_kickable = other._is_kickable;
    _is_piercing = other._is_piercing;
    return *this;
}

void Indie::Bomb::init()
{
    _bomb_node = SceneContent::getSceneMgr()->addAnimatedMeshSceneNode(Indie::MeshLoader::getMesh(Indie::MeshLoader::BOMB));
    if (!_bomb_node)
        throw BombError(ERR_LOADING_TEXTURE, __FILE__, __LINE__);
    _bomb_node->setMaterialTexture(0, SceneContent::getDriver()->getTexture(BOMB_TEXTURE_PATH));
    _bomb_node->setVisible(false);
    _bomb_node->setScale(irr::core::vector3df{ 0.10, 0.10, 0.10});
    _bomb_node->setRotation(irr::core::vector3df{ -90, 0, 0});
    _audio.setBuffer(*SfxBank::get().getSoundBuffer(SFX_EXPLOSION).get());
}

void Indie::Bomb::update()
{
    if (_is_droped && _clock.getElapsedTimeAsSeconds() >= DEFAULT_BOMB_EXPLODE_TIME) {
        MapDrawer::get().breakBlockAt(_bomb_node->getPosition(), _radius, _is_piercing);
        _audio.play();
        recover();
    }
}

bool Indie::Bomb::isDroped() const
{
    return _is_droped;
}

void Indie::Bomb::drop(irr::core::vector3df bomb_pos)
{
    _bomb_node->setPosition(MapDrawer::getRightBlockPosition(bomb_pos));
    _bomb_node->setVisible(true);
	Indie::ParticleManager::get().createParticle(Particle::BOMB_SMOKE, _bomb_node->getPosition(), DEFAULT_BOMB_EXPLODE_TIME_MS);
    _is_droped = true;
    _clock.restart();
}

void Indie::Bomb::recover()
{
    _is_droped = false;
    _bomb_node->setVisible(false);
}

void Indie::Bomb::onCollision(const Direction &direction)
{
    if (_is_kickable)
        Log::console("Bomb onCollision : kick bomb\n");
}

void Indie::Bomb::addToRadius(int to_add)
{
    _radius += to_add;
}

void Indie::Bomb::setKickable(bool is_kickable)
{
    _is_kickable = is_kickable;
}

void Indie::Bomb::setPiercing(bool is_piercing)
{
    _is_piercing = is_piercing;
}

std::size_t Indie::Bomb::getRadius() const
{
    return _radius;
}

Indie::IBomb *Indie::Bomb::clone() const
{
    return BombFactory::create(IBomb::Default, nullptr);
}

Indie::AddonBomb::AddonBomb(std::shared_ptr<IBomb> bomb)
    : _bomb(bomb)
{
}

Indie::AddonBomb::AddonBomb(IBomb *bomb)
{
    _bomb.reset(bomb);
}

Indie::AddonBomb::AddonBomb(const AddonBomb &other)
{
    *this = other;
}

Indie::AddonBomb &Indie::AddonBomb::operator=(const AddonBomb &other)
{
    _bomb = other._bomb;
    return *this;
}

void Indie::AddonBomb::init()
{
    _bomb->init();
}

void Indie::AddonBomb::update()
{
    _bomb->update();
}

bool Indie::AddonBomb::isDroped() const
{
    return _bomb->isDroped();
}

void Indie::AddonBomb::drop(irr::core::vector3df bomb_pos)
{
    _bomb->drop(bomb_pos);
}

void Indie::AddonBomb::recover()
{
    _bomb->recover();
}

void Indie::AddonBomb::onCollision(const Direction &direction)
{
    _bomb->onCollision(direction);
}

void Indie::AddonBomb::addToRadius(int to_add)
{
    _bomb->addToRadius(to_add);
}

void Indie::AddonBomb::setKickable(bool is_kickable)
{
    _bomb->setKickable(is_kickable);
}

void Indie::AddonBomb::setPiercing(bool is_piercing)
{
    _bomb->setPiercing(is_piercing);
}

std::size_t Indie::AddonBomb::getRadius() const
{
    return _bomb->getRadius();
}

Indie::BombRadiusUp::BombRadiusUp(std::shared_ptr<IBomb> bomb)
    : AddonBomb(bomb)
{
    if (bomb->getRadius() < MAX_RADIUS)
        addToRadius();
}

Indie::BombRadiusUp::BombRadiusUp(IBomb *bomb)
    : AddonBomb(bomb)
{
    if (bomb->getRadius() < MAX_RADIUS)
        addToRadius();
}

Indie::IBomb *Indie::BombRadiusUp::clone() const
{
    return BombFactory::create(IBomb::RadiusUp, _bomb->clone());
}

std::shared_ptr<Indie::IBomb> Indie::BombFactory::create(const IBomb::BombType &bomb_type, std::shared_ptr<IBomb> bomb)
{
    BombFactoryMap factory_map = init();

    if (factory_map.find(bomb_type) == factory_map.end())
        throw BombError(ERR_UNKNOWN_BOMB_TYPE, __FILE__, __LINE__);
    return factory_map[bomb_type](bomb);
}

Indie::BombFactory::BombFactoryMap Indie::BombFactory::init()
{
    static bool is_setup = false;
    static BombFactoryMap factory_map;

    if (is_setup)
        return factory_map;
    factory_map.emplace(IBomb::Default,     [] (std::shared_ptr<IBomb> &bomb) -> std::shared_ptr<IBomb> { (void)bomb; return std::make_shared<Bomb>(); });
    factory_map.emplace(IBomb::RadiusUp,    [] (std::shared_ptr<IBomb> &bomb) -> std::shared_ptr<IBomb> { return std::make_shared<BombRadiusUp>(bomb); });
    is_setup = true;
    return factory_map;
}

Indie::IBomb *Indie::BombFactory::create(const IBomb::BombType &bomb_type, IBomb *bomb)
{
    static bool is_setup = false;
    static std::map<IBomb::BombType, std::function<IBomb *(IBomb *)>> factory_map;

    if (!is_setup) {
        factory_map.emplace(IBomb::Default,     [] (IBomb *bomb) -> IBomb * { (void)bomb; return new Bomb; });
        factory_map.emplace(IBomb::RadiusUp,    [] (IBomb *bomb) -> IBomb * { return new BombRadiusUp(bomb); });
        is_setup = true;
    }

    if (factory_map.find(bomb_type) == factory_map.end())
        throw BombError(ERR_UNKNOWN_BOMB_TYPE, __FILE__, __LINE__);
    return factory_map[bomb_type](bomb);
}