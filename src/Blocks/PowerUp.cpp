/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** PowerUp
*/

#include "blocks/PowerUp.hpp"
#include "core/Scene.hpp"
#include "core/Particles.hpp"
#include "core/MeshLoader.hpp"

Indie::PowerUp &Indie::PowerUp::get()
{
	static PowerUp obj;
	return obj;
}

void Indie::PowerUp::tryCreate(const irr::core::vector3df &blockPosition)
{
	get().tryCreateImpl(blockPosition);
}

void Indie::PowerUp::check(Character *player)
{
	get().checkImpl(player);
}

void Indie::PowerUp::tryCreateImpl(const irr::core::vector3df &blockPosition)
{
	int odds = std::rand() % POWER_UP_PROBA;

	if (!odds)
		choosePowerUp(blockPosition);
}

void Indie::PowerUp::checkImpl(Character *player)
{
	irr::core::vector3df playerPosition = MapDrawer::getRightBlockPosition(player->getPosition());

	for (unsigned int i = 0; i != _powerUps.size(); i += 1) {
		irr::core::vector3df powerUpPosition = MapDrawer::getRightBlockPosition(_powerUps[i]->getPosition());
		if (powerUpPosition.X == playerPosition.X && powerUpPosition.Z == playerPosition.Z) {
			addPowerUp(player, i);
			_powerUps[i]->setVisible(false);
			_powerUps.erase(_powerUps.begin() + i);
			_types.erase(_types.begin() + i);
			i -= 1;
		}
	}
}

void Indie::PowerUp::choosePowerUp(const irr::core::vector3df &blockPosition)
{
	static bool isSetup = false;
	static std::map<PowerUpType, std::function<void(const irr::core::vector3df &)>> tmp;

	if (!isSetup) {
		tmp.emplace(RadiusUp,   [&] (const irr::core::vector3df &blockPosition)
		{
			irr::core::vector3df scale = { 0.32, 0.32, 0.32 };
			irr::core::vector3df rotation = { -90, 0, 0 };

			_powerUps.emplace_back(SceneContent::getSceneMgr()->addAnimatedMeshSceneNode(MeshLoader::getMesh(MeshLoader::FLOWER_MODEL)));
			_powerUps.back()->setMaterialTexture(0, SceneContent::getDriver()->getTexture(FLOWER_TEXTURE));
			_powerUps.back()->setPosition(MapDrawer::getRightBlockPosition(blockPosition));
			_powerUps.back()->setMaterialFlag(EMF_LIGHTING, false);
			_powerUps.back()->setScale(scale);
			_types.emplace_back(RadiusUp);
		});
		tmp.emplace(AddBomb,    [&] (const irr::core::vector3df &blockPosition)
		{
			irr::core::vector3df scale = { 0.07, 0.07, 0.07 };
			irr::core::vector3df rotation = { -90, 0, 0 };

			_powerUps.emplace_back(SceneContent::getSceneMgr()->addAnimatedMeshSceneNode(MeshLoader::getMesh(MeshLoader::BOMB)));
			_powerUps.back()->setMaterialTexture(0, SceneContent::getDriver()->getTexture(BOMB_TEXTURE_PATH));
			_powerUps.back()->setPosition(MapDrawer::getRightBlockPosition(blockPosition));
			_powerUps.back()->setMaterialFlag(EMF_LIGHTING, false);
			_powerUps.back()->setScale(scale);
			_powerUps.back()->setRotation(rotation);
			_types.emplace_back(AddBomb);
		});
		isSetup = true;
	}

	auto item = tmp.begin();
	std::advance(item, std::rand() % tmp.size());
	item->second(blockPosition);
}

void Indie::PowerUp::addPowerUp(Character *player, unsigned int index)
{
	static bool isSetup = false;
	static std::map<PowerUpType, std::function<void()>> tmp;

	ParticleManager::get().createParticle(Particle::POWER_UP_EFFECT, player->getPosition(), 150);
	_audio.setBuffer(*SfxBank::get().getSoundBuffer(SFX_POWER_UP).get());
	_audio.play();
	if (!isSetup) {
		tmp.emplace(RadiusUp,   [&player] { player->bombLevelUp(IBomb::RadiusUp); });
		tmp.emplace(AddBomb,    [&player] { player->addBomb(); });
		isSetup = true;
	}

	tmp[_types[index]]();
}