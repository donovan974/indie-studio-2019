/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** input
*/

#include "core/Scene.hpp"
#include "core/MeshLoader.hpp"
#include "decoration/Grass.hpp"

Indie::Grass::Grass()
{
}

void Indie::Grass::init()
{
	IrrlichtDevice* Device = Indie::SceneContent::getDevice();
	_mesh = MeshLoader::getMesh(MeshLoader::GRASS);
	_node = Device->getSceneManager()->addAnimatedMeshSceneNode(_mesh);
	_node->setMaterialTexture(0, Device->getVideoDriver()->getTexture(GRASS_TEXTURE));
	_node->setMaterialFlag(EMF_LIGHTING, false);
	_node->setScale(vector3df(1, 1, 1));
	_node->setPosition(vector3df(0, -60, 0));
	_node->setRotation(vector3df(-90, 0, 0));
}