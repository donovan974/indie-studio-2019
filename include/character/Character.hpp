/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** input
*/

#pragma once

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "Irrlicht.lib")
#endif

#include <vector>
#include "character/ICharacter.hpp"
#include "character/Bomb.hpp"
#include "character/Keys.hpp"
#include "core/SfxBank.hpp"

using namespace io;
using namespace irr;
using namespace gui;
using namespace core;
using namespace video;
using namespace scene;

namespace Indie {


	class CharacterError : public Error {
		public:
			CharacterError(const char *msg, const char *file, int line)
				: Error(msg, file, line) {}
			~CharacterError() = default;
	};

	#define GFX_BOMB			"../media/bomb_logo.resized.png"

	#define ERR_HP 				"hp must be positive."
	#define ERR_MODEL			"Could not load model."
	#define ERR_TEXTURE 		"Could not load texture."
	#define ERR_PLAYER_UNKNOWN	"Player unknown."

	#define MAX_PLAYER	2
	#define MAX_BOMB	3
	#define MAX_RADIUS  3

	#define PLAYER_ONE_START_POSITION   irr::core::vector3df{ 5,  0, 5  }
	#define PLAYER_TWO_START_POSITION   irr::core::vector3df{ 55, 0, 55 }
	#define PLAYER_THREE_START_POSITION irr::core::vector3df{ 55, 0, 0  }
	#define PLAYER_FOUR_START_POSITION  irr::core::vector3df{ 5,  0, 55 }

	#define PLAYER_ONE_GUI_POSITION   	(irr::core::recti(700,  80, 720, 100))
	#define PLAYER_TWO_GUI_POSITION   	(irr::core::recti(700, 510, 720, 550))
	#define PLAYER_THREE_GUI_POSITION   (irr::core::recti( 50, 510,  70, 550))
	#define PLAYER_FOUR_GUI_POSITION   	(irr::core::recti( 50,  80,  70, 100))


	class CharacterGUI{
		public:

			CharacterGUI(const char *key);
			~CharacterGUI() = default;

			void setBomNumber(irr::u32 i) { _bombNumber  = i;}
			void setBomRadius(irr::u32 i) {_radiusNumber = i;}

			void init();
			void update();

		protected:
		private:
			unsigned int _bombNumber;
			unsigned int _radiusNumber;
			irr::core::recti _origin;
			irr::gui::IGUIStaticText *_bombLabel;
			irr::gui::IGUIStaticText *_radiusLabel;
	};


	class Character : public ICharacter {
		public:
			Character(const char *player);

			void init();
			void update();
			f32 TopLimitX(vector3df&, f32);
			f32 TopLimitZ(vector3df&, f32);
			void RotationHandling(const int);
			void RotateAction(vector3df& velocity, f32 frameTime);
			bool CheckLife() const { return _hp > 0 ? true : false; };

			void setSpeed(int speed) { _speed = speed; };
			void setPosition(vector3df position) { _node->setPosition(position); };
			void setHp(int hp) { hp > 0 ? _hp = hp : throw CharacterError(ERR_HP, __FILE__, __LINE__); };

			int getHp() const { return _hp; };
			int getSpeed() const { return _speed; };
			unsigned int getBombNumber() {return _bombStock.size();}
			vector3df getPosition() const { return _node->getPosition(); };
			IAnimatedMeshSceneNode* getAMnode() { return _node; }

			void MoveHandling(KeyHandling, f32, vector3df &);
			void MovePlayer(IrrlichtDevice*, KeyHandling);

			void placeBomb(KeyHandling keys);
			void getKilled();
			void bombLevelUp(const IBomb::BombType &bombType);
			void addBomb(const std::size_t &nb_bomb = 1);
			void activeMovement(bool isActive) {_isActive = isActive;}


		protected:
			int _hp;
			u32 _then;
			int _speed;
			bool _isAnime;
			bool _isActive;
			bool _isWalking;
			sf::Sound _audio;
			vector3df _NodeP;
			CharacterGUI _gui;
			IAnimatedMesh* _mesh;
			std::vector<bool> _states;
			IAnimatedMeshSceneNode* _node;
			const f32 MOVEMENT_SPEED = 5.f;
			const char *_texture = "../media/Bomberman.png";
			std::vector<std::shared_ptr<IBomb>> _bombStock;
			bool _isalive = true;
			CharacterKeys _keys;
			Chrono _movementClock;
	};

	class CharacterContainer {
		public:

			CharacterContainer(const CharacterContainer &) = delete;
			CharacterContainer &operator=(const CharacterContainer &) = delete;
			~CharacterContainer() = default;

			static CharacterContainer &get();
			static void init(std::size_t nb_player);
			static void update();
			static void checkDeath(const irr::core::vector3df &position);

			void initImpl(std::size_t nb_player);
			void updateImpl();
			void checkDeathImpl(const irr::core::vector3df &position);

		private:

			struct Entity {
				public:
					std::shared_ptr<ICharacter> character;
					bool isplayer;
			};

			CharacterContainer() = default;
			irr::core::vector3df getStartPosition(const char *player);

		private:
			Indie::Chrono _chrono;
			std::vector<Entity> _characters;
	};

}
