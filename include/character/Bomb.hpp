/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** Bomb
*/

#ifndef BOMB_HPP_
#define BOMB_HPP_

#include "IndieStudio.hpp"
#include "tools/Chrono.hpp"
#include "core/SfxBank.hpp"

#define BOMB_TEXTURE_PATH   "../media/bomb/bomb.png"
#define BOMB_OBJECT_PATH    "../media/bomb/Bomb.obj"

namespace Indie {

    #define DEFAULT_BOMB_RADIUS             1
    #define DEFAULT_BOMB_KICKABLE           false
    #define DEFAULT_BOMB_PIERCING           false
    #define DEFAULT_BOMB_EXPLODE_TIME       2
    #define DEFAULT_BOMB_EXPLODE_TIME_MS    ((DEFAULT_BOMB_EXPLODE_TIME) * (1000))

    #define MAX_RADIUS                  3

    enum Direction {
        North = 0,
        South,
        West,
        Est
    };

    class IBomb : public IObject {
        public:

            enum BombType {
                Default = 0,
                RadiusUp
            };

            virtual ~IBomb() = default;
            virtual void init() = 0;
            virtual void update() = 0;
            virtual bool isDroped() const = 0;
            virtual void drop(irr::core::vector3df bomb_pos) = 0;
            virtual void recover() = 0;
            virtual void onCollision(const Direction &direction) = 0;
            virtual void addToRadius(int to_add = 1) = 0;
            virtual void setKickable(bool is_kickable = true) = 0;
            virtual void setPiercing(bool is_piercing = true) = 0;
            virtual std::size_t getRadius() const = 0;
            virtual IBomb *clone() const = 0;

    };

    class Bomb : public IBomb {
        public:

            Bomb() = default;
            Bomb(const Bomb &other);
            Bomb &operator=(const Bomb &other);
            ~Bomb() = default;

            void init();
            void update();
            bool isDroped() const;
            void drop(irr::core::vector3df bomb_pos);
            void recover();
            void onCollision(const Direction &direction);
            void addToRadius(int to_add = 1);
            void setKickable(bool is_kickable = true);
            void setPiercing(bool is_piercing = true);
            std::size_t getRadius() const;
            IBomb *clone() const;

        private:

            bool _is_droped = false;
            std::size_t _radius = DEFAULT_BOMB_RADIUS;
            bool _is_kickable = DEFAULT_BOMB_KICKABLE;
            bool _is_piercing = DEFAULT_BOMB_PIERCING;
            irr::scene::IAnimatedMeshSceneNode *_bomb_node;
            sf::Sound _audio;
            Chrono _clock;
    };

    class AddonBomb : public IBomb {
        public:

            AddonBomb(std::shared_ptr<IBomb> bomb);
            AddonBomb(IBomb *bomb);
            AddonBomb(const AddonBomb &other);
            AddonBomb &operator=(const AddonBomb &other);
            ~AddonBomb() = default;

            void init();
            void update();
            bool isDroped() const;
            void drop(irr::core::vector3df bomb_pos);
            void recover();
            void onCollision(const Direction &direction);
            void addToRadius(int to_add = 1);
            void setKickable(bool is_kickable = true);
            void setPiercing(bool is_piercing = true);
            std::size_t getRadius() const;
            virtual IBomb *clone() const = 0;

        protected:

            std::shared_ptr<IBomb> _bomb;
    };


    class BombRadiusUp : public AddonBomb {
        public:

            BombRadiusUp(std::shared_ptr<IBomb> bomb);
            BombRadiusUp(IBomb *bomb);
            ~BombRadiusUp() = default;

            IBomb *clone() const;
    };

    class BombFactory {
        public:

            BombFactory() = delete;
            BombFactory(const BombFactory &) = delete;
            BombFactory &operator=(const BombFactory &) = delete;
            ~BombFactory() = delete;

            using BombFactoryMap = std::map<IBomb::BombType, std::function<std::shared_ptr<IBomb>(std::shared_ptr<IBomb> &bomb)>>;

            static std::shared_ptr<IBomb> create(const IBomb::BombType &bomb_type, std::shared_ptr<IBomb> bomb);
            static IBomb *create(const IBomb::BombType &bomb_type, IBomb *bomb);

        private:

            static BombFactoryMap init();
    };

    class BombError : public Error {
        public:
            BombError(const char *msg, const char *file, int line)
                : Indie::Error(msg, file, line) {}
            ~BombError() = default;
    };

    #define ERR_UNKNOWN_BOMB_TYPE   "Unknown bomb type."
    #define ERR_LOADING_TEXTURE     "Couldn't load texture."

}

#endif