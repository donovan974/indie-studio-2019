/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** DestroyableBlock
*/

#include "blocks/DestroyableBlock.hpp"

Indie::DestroyableBlock::DestroyableBlock() :
    Block::Block(5)
{
    _type = DESTROYABLE;
    _textures.emplace_back(DESTROYABLE_TEXTURE_1);
    _textures.emplace_back(DESTROYABLE_TEXTURE_2);
}

Indie::DestroyableBlock::~DestroyableBlock()
{
}

void Indie::DestroyableBlock::init()
{
    Block::init();
}

void Indie::DestroyableBlock::update()
{
    Block::update();
}

irr::scene::IMeshSceneNode *Indie::DestroyableBlock::getNode()
{
    return Block::getNode();
}