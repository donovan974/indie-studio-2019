/*
** EPITECH PROJECT, 2020
** indie-studio-2019
** File description:
** Particles
*/

#ifndef PARTICLES_HPP_
#define PARTICLES_HPP_

#include "IndieStudio.hpp"
#include "tools/Chrono.hpp"

#define FIRE_PARTICLE_PATH "../media/fireball.bmp"

namespace Indie {

class Particle {
	public:

		enum Type{
			BOMB_SMOKE = 0,
			POWER_UP_EFFECT
		};

		Particle(irr::scene::IParticleSystemSceneNode *irrParticle);
		~Particle() = default;

		void setDuration(unsigned int duration) {_time_duration = duration;}
		bool update();

	protected:
	private:
		Indie::Chrono _time;
		unsigned int _time_duration;
		irr::scene::IParticleSystemSceneNode *_particle;
};

class ParticleManager {
	public:
		~ParticleManager() = default;
		ParticleManager(const ParticleManager &) = delete;
		ParticleManager &operator=(const ParticleManager &) = delete;

		static ParticleManager &get() {static ParticleManager p; return p;}
		static void update()    	  {get().updateParticles();}

		void createParticle(const Particle::Type type, const irr::core::vector3df &position, unsigned int time_duration);
		void updateParticles();

	protected:
	private:
		ParticleManager() = default;
		std::vector<std::shared_ptr<Particle>> _particlesMap;
};

class ParticleFactory {
	public:
		static std::shared_ptr<Particle> make_particle(const Particle::Type, const irr::core::vector3df &position);

	private:
		static std::shared_ptr<Particle> getPowerUpEffect(const irr::core::vector3df &position);
		static std::shared_ptr<Particle> getBombSmoke(const irr::core::vector3df &position);
};

}

#endif /* !PARTICLES_HPP_ */
